<?php
include "db_user.php";
if (isset($_POST["submit"])) {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $gender = $_POST["gender"];
    $email = $_POST["email"];
    $sql = "INSERT INTO `crud`(`id`, `first_name`,
     `last_name`, `email`, `gender`) VALUES (NULL,'$first_name','$last_name','$email','$gender')
    ";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        header("Location:index.php?msg=New record created successsfully");
    } else {
        echo "failed:" . mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP CRUD Aplication</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <nav class="navbar navbar-light justify-content-center fs-3 mb-5" style="background-color:#00ff5573">
        PHP Complete CRUD Application
    </nav>
    <div class="container">
        <div class="text-center mb-4">
            <h3>Add New User</h3>
            <p class="text-muted">Complete the form below to add a new user</p>
        </div>
        <div class="container d-flex justify-content-center">
            <form action="" method="post" style="width:50vw;min-width:300px">
                <div class="row">
                    <div class="col">
                        <label class="form-label">
                            First name:
                        </label>
                        <input type="text" class="form-control" name="first_name" placeholder="Albert">
                    </div>
                    <div class="col">
                        <label class="form-label">
                            Last name:
                        </label>
                        <input type="text" class="form-control" name="last_name" placeholder="Eistein">
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label">
                        Email:
                    </label>
                    <input type="email" class="form-control" name="email" placeholder="name@email.com">
                </div>
                <div class="form-group mb-3">
                    <label>Gender:</label> &nbsp;
                    <div class="ml-3">
                        <input type="radio" class="form-check-input mt-2" name="gender" id="male" value="male">
                        <label for="male" class="form-input-label">male</label>
                        &nbsp;
                    </div>
                    <div class="ml-3">
                        <input type="radio" class="form-check-input mt-2" name="gender" id="female" value="female">
                        <label for="female" class="form-input-label">female</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-success" name="submit">Save</button>
                <a href="index.php" class="btn btn-danger">Cancel</a>
            </form>
        </div>
    </div>





    <script src=" https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>